package de.hfu;
import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        System.out.println( "OSSE Praktikum Projekt");
        System.out.println("Eingabe:");
        Scanner scanner = new Scanner(System.in);
        String cache = scanner.nextLine();
        System.out.println("Ausgabe in Großbuchstaben: " + cache.toUpperCase());
    }
}