package de.hfu;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;

public class BaseResidentServiceTest {

    private ResidentRepositoryStub createResidentRepositoryStub() {
        List<Resident> residentlist = new ArrayList<>();
        residentlist.add(new Resident("Kevin", "Mais", "Bregweg", "Berlin", new Date()));
        residentlist.add(new Resident("Kev", "Klein", "Haupstraße", "Furtwangen", new Date()));
        residentlist.add(new Resident("Lukas", "Klein", "Schlehenweg", "Bonn", new Date()));
        return new ResidentRepositoryStub(residentlist);
    }

    @Test
    public void testGetFilteredResidentsList() {
        
        ResidentRepository residentRepositoryStub = createResidentRepositoryStub();
        BaseResidentService baseResidentService = new BaseResidentService();

        baseResidentService.setResidentRepository(residentRepositoryStub);

        // Test 1
        Resident person1 = new Resident("Kevin", null, null, null, null);
        List<Resident> list1 = baseResidentService.getFilteredResidentsList(person1);
        assertEquals(1, list1.size());

        // Test 2
        Resident person2 = new Resident(null, "Mais", "Bregweg", null, null);
        List<Resident> list2 = baseResidentService.getFilteredResidentsList(person2);
        assertEquals(1, list2.size());

        // Test 3
        Resident person3 = new Resident(null, "Klein", null, null, null);
        List<Resident> list3 = baseResidentService.getFilteredResidentsList(person3);
        assertEquals(2, list3.size());

        // Test 4
        Resident person4 = new Resident("Kevin", "Mais", "Bregweg", "Berlin", new Date());
        List<Resident> list4 = baseResidentService.getFilteredResidentsList(person4);
        assertEquals(1, list4.size());

        // Test 5
        Resident person5 = new Resident(null, null, null, null, null);
        List<Resident> list5 = baseResidentService.getFilteredResidentsList(person5);
        assertEquals(3, list5.size());
    }

    @Test
    public void testGetUniqueResident() throws ResidentServiceException {
        
        ResidentRepository residentRepositoryStub = createResidentRepositoryStub();
        BaseResidentService baseResidentService = new BaseResidentService();

        baseResidentService.setResidentRepository(residentRepositoryStub);

        // Test 1
        Resident person1 = new Resident("Kevin","Mais", null, null, null);
        Resident list1 = baseResidentService.getUniqueResident(person1);
        assertEquals("Kevin", list1.getGivenName());
        assertEquals("Mais", list1.getFamilyName());

        // Test 2
        Resident person2 = new Resident(null, null, "Schlehenweg", null, null);
        Resident list2 = baseResidentService.getUniqueResident(person2);
        assertNotSame("Bregweg", list2.getStreet());
        assertNotSame("Haupstraße", list2.getStreet());
        
        // Test 3
        Resident person3 = new Resident(null,"Klein", null, null, null);
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(person3);}, "ResidentServiceException was expected");

        // Test 4
        Resident person4 = new Resident(null, null, null, "Furtwangen", null);
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(person4);}, "ResidentServiceException was expected");

        // Test 5
        Resident person5 = new Resident("*", null, null, null, new Date());
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(person5);}, "ResidentServiceException was expected");
    }

}

