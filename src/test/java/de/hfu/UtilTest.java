package de.hfu;

import de.hfu.unit.Util;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test der Klasse Util")
public class UtilTest {
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5,6})
    void testDerMonateTrue(int monat){
        assertTrue(Util.istErstesHalbjahr(monat), "true");
    }

    @ParameterizedTest
    @ValueSource(ints = {10, 11, 12, 8, 9, 7})
    void testDerMonateFalse(int monat){
        assertFalse(Util.istErstesHalbjahr(monat), "false");
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 13, 42, 666, 14})
    void testDerMonateException(int monat){
        assertThrows(IllegalArgumentException.class, () -> {Util.istErstesHalbjahr(monat);}, "IllegalArgumentException was expected");
    }

}
