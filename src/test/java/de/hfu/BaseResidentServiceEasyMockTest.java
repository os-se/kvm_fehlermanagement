package de.hfu;

import java.util.Collections;
import java.util.Date;

import org.junit.jupiter.api.Test;
import static org.easymock.EasyMock.*;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;

public class BaseResidentServiceEasyMockTest {
    
    @Test
    public void testGetUniqueResident() throws ResidentServiceException {
        
        Resident expected = new Resident("Kevin", "Mais", "Bregweg", "Berlin", new Date());

        ResidentRepository residentRepositoryMock = createMock(ResidentRepository.class);
        
        expect(residentRepositoryMock.getResidents()).andReturn(Collections.singletonList(expected));
        replay(residentRepositoryMock);
        
        BaseResidentService baseResidentService = new BaseResidentService();
        baseResidentService.setResidentRepository(residentRepositoryMock);

        Resident filterResident = new Resident("Kevin", "Mais", null, null, null);
        baseResidentService.getUniqueResident(filterResident);

        verify(residentRepositoryMock);
    }
}

