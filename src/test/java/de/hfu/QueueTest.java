package de.hfu;

import de.hfu.unit.Queue;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test der Klasse Queue")
public class QueueTest {

    @Test
    void testKonstruktor(){
        assertThrows(IllegalArgumentException.class, () -> {Queue test = new Queue(-3);}, "IllegalArgumentException was expected");
    } 
    
    @Test
    void testEnqueue(){
        Queue test = new Queue(3);
        test.enqueue(1);
        test.enqueue(2);
        test.enqueue(3);
        assertEquals(test.dequeue(), 1);
        assertEquals(test.dequeue(), 2);
        assertEquals(test.dequeue(), 3);
        test.enqueue(1);
        test.enqueue(2);
        test.enqueue(3);
        test.enqueue(4);
        assertEquals(test.dequeue(), 1);
        assertEquals(test.dequeue(), 2);
        assertEquals(test.dequeue(), 4);
    }

    @Test
    void testDequeue(){
        Queue test = new Queue(3);
        assertThrows(IllegalStateException.class, () -> {test.dequeue();}, "IllegalStateException was expected");
        test.enqueue(1);
        test.enqueue(2);
        test.enqueue(3);
        test.dequeue();
        test.dequeue();
        test.enqueue(4);
        test.enqueue(5);
        assertEquals(test.dequeue(), 3);
        assertEquals(test.dequeue(), 4);
        assertEquals(test.dequeue(), 5);
    }

}
